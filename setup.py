import setuptools

with open('README.md', 'r') as fh:
    long_description = fh.read()

setuptools.setup(
    name='zgrpc',
    version_format='{tag}.dev{commitcount}+{gitsha}',
    setup_requires=['setuptools-git-version'],
    author='Jesus Diaz',
    author_email='jesus.diaz@zenpagos.com',
    description='This package contains gRPC definitions for all services',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/zenpagos/protorepo',
    packages=setuptools.find_packages(),
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ],
)
