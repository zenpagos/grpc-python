#!make

package:
	find . -type f -name '*.py' -exec sed -i '' 's/^\(import.*pb2\)/from . \1/g' {} \;
	pip install -r requirements.txt
	s3pypi --bucket pypi.zenpagos.com --force
	rm -rf build dist zgrpc.egg-info

cleanup:
	rm -rf build dist zgrpc.egg-info
